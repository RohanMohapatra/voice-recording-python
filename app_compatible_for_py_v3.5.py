
# coding: utf-8

# ### Written in Python 3.5 

# # Import Utility Programs

# In[ ]:


import record
import playaudio


# # Important Defintions

# In[ ]:


userName = input("Enter Speaker Name:")
fileDest = 'Recordings/'
secs=0
lang = "kan"
z=1
srcFile = '1-100.txt'


# ## Read the transcript

# In[ ]:


with open(srcFile) as file1:
    lines = file1.readlines()


# ## Loop to record data

# In[ ]:


for i in range(0,len(lines)):
    print("Word to be Recorded: "+ lines[i])
    ch = input("Enter \n c. If you are ready \n v. To exit\n")
    if(ch == 'c'):
        #print("Wait in silence to begin recording; wait in silence to terminate")
        record.record_to_file(fileDest+lang+"-"+str(z)+userName)
        print("done - result written")
        #record.record_audio(fileDest+lang+"-"+str(z)+userName)
        done = 1
        while(done):
            choice = 0
            choice = input("Enter \nc. To play \nv. To record it again \nb. If done \n")
            if(choice == 'c'):
                playaudio.playaudio(fileDest+lang+"-"+str(z)+userName+".wav")
            elif(choice == 'v'):
                #print("Wait in silence to begin recording; wait in silence to terminate")
                print("Word to be Recorded: "+ lines[i])
                record.record_to_file(fileDest+lang+"-"+str(z)+userName)
                print("done - result written")
            elif(choice == 'b'):
                break;
    elif(ch=='v'):
        break;
    z=z+1


# ## Final Message

# In[ ]:


print("\nCongratulations...!! All the recordings got completed...!!\n")


# In[ ]:




