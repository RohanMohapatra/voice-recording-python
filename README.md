# Voice Recording App for Kannada Language
- Written in Python 3.5

## Dependencies
- pyaudio -> pip3 install pyaudio
- wave -> pip3 install wave

## It's in Beta Stage

## How to Run?
- python app_compatible_for_py_v3.5.py
- record.py and playaudio.py are helper classes.

## Developed by Rohan Mohapatra