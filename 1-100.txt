amErika saMyukta saMsthAna saMvidhAna dinAcaraNe
keribbiyan samudra da pUrva BAgadalli iruva oMdu dvIpa rAZhTra
uttara amErika KaMDada atyaMta cikka mattu atyaMta kaDime janasaMKye hoMdiruva dESa idu
Ore akZharagaLu hIge kANisuttade Ore akZharagaLu
SivarAjkumAr avara patniya hesaru gItA SivarAjkumAr
PilippIns gaNarAjya AgnEya EZhyA dalliruva oMdu dvIpa gaLa dESa
audyOgika kZhEtradalli avara koDuge ananya
amErikAda vAZhiMgTan dalliruva kAvEri sAMskRutika saMstheya adhyakZharAgiddaru
maisUru viSvavidyAlayavu BAratada oMdu pramuKa viSvavidyAlaya
TeMplET eMdare Enu I praSne kelavu Odugarige barabahudu
oMdu sarkAri prAthamika mattu prauDha SAle ide
saMyukta karnATaka karmavIra kastUri patrikegaLige saMpAdakarAgiddaru
ivaralli kelavara hesarugaLannu mAtra illi koDalAgide
maThagaLu hesarina lEKanavannu nIvu prAraMBisabahudu
muGal siMhAsanada mEle tanna sOdara saMbaMdhi
aitihAsikavAgi prasiddhavAda anEka sthaLagaLu illive
AddariMda I puTa hora baMtu
carc gET na pradhAna railve kaCEri
jAnapada taj~ja jAnapada akADemi praSasti rAjyOtsava praSasti modalAda praSastigaLu laBisive
maDikEri koDagu jilleya oMdu tAlUku hAgU jillA kEMdra
paScima baMgALada prathama mahiLA muKyamaMtri
kannaDa da hesarAMta calanacitra nirdESaka e vi SEZhagirirAv
mUlataH uttara paScima mattu madhya yurOp
A citragaLannu kannaDa vikipIDiyadalli upayOgisuvudu sulaBa
nirNaya praSne puTa bahaLa oLLeya prayatna
prAcIna citradurga pi hec Di mahA prabaMdha
mI dUradalliruva karnATaka da oMdu aitihAsika sthaLa
yahUdi dharma dallina pramuKa dhArmika graMtha
SrI yavara jotege duDida geLeyaru pro narasiMhAcAr kastUri raMgacAr muMtAdavarugaLu
ivara saMgIta nirdESanada modala kannaDa citra E
rAjakumAr jIvana hAgU sAdhane kuritu hlAdarAv bareda baMgArada manuZhya atyaMta janapriya pustaka
idaralli halavu lEKakaru saMkRuta mattu kannaDadalli bareyuttiddAre
viZhNuvardhan aBinayada kelavu prasiddha citragaLu
Adare iMtaha vidyamAna balu aparUpa
idu halmiDi SAsana eMdu prasidda
maurya sAmrAjyada ati prasiddha cakravarti sAmrAT aSOka ADaLita
kannaDa da janapriya lEKaki uZhA navaratnarAM
hiMdU dharmada sauramAna paMcAMgada modala mAsa
idu kelavu haLeya sadasyara anuBava
Adare adara bagge araLikaTTeyalli baredare uttama
hesaru tappAgi I puTada avaSyakate Iga iruvudilla
harihara Uru dAvaNagere jille oMdu tAllUku kEMdra
bi ji el svAmiyavaru bareda pustaka
dakZhiNa kannaDa jilleya oMdu dhArmika tANa
idu kannaDa vikipIDiyada samudAya puTa
idu oMdu kZhAra Basma lOha
siK dharma da bagge lEKanagaLa varga
bAlasubrahmaNyaM maMjuLA gururAj latA haMsalEKa
uZhA navaratnarAM avara idE hesarina kAdaMbari AdhArita
sEnA puraskAragaLu hesarina lEKanavannu nIvu prAraMBisabahudu
I kRuti DA SivarAma kAraMtaru bareda sAmAjika kAdaMbarigaLalli oMdu
uttaradalli BImA nadi mattu dakZhiNadalli kRuZhNA nadigaLu hariyuttade
kannaDa citraraMgada innobba janapriya naTa SrInAth samIpada baMdhu
I mElina eraDu sAlugaLu namma nimma carce puTakke heccu sUkta
I maradiMda auZhadha tayArike bagge saMSOdhane naDeyuttide
BArata athavA iMDiyA eMdu kareyalpaDuva dakZhiNa EZhyA da dESa
hoysaLa vaMSada rAja kUDA viZhNuvardhananE eMdu avaru tamma naDeyannu samarthisikoMDaru
rAjkumAr kannaDa citraraMga da pramuKa naTa
hAsana jilleya aitihAsika dhArmika pravAsi tANa
sthaLa Sivamogga jilleya sAgara tAlUku
modala mattu eraDaneya maisUru yuddha
uttarada himAlaya pradESadalli TibeT saMskRutiya CApu kaMDubaruvudu
upEMdra nirdESanada kannaDa calanacitra OM bagge mAhitige I lEKana nODi
maThagaLu padavannu bEre lEKanagaLalli huDuki
moGal sAmrAjya hesarina lEKanavannu nIvu prAraMBisabahudu
beMgaLUrina BAratIya vij~jAna saMstheyu oleyannu vinyAsapaDisittu
PrAns sarakArada atyunnata nAgarika puraskAra nIDalAgide
amErika saMyukta saMsthAnada rAZhTrapatigaLu amErika dESada sarakArada adhyakZharu
ananya oMdu kannaDa BAZheya pada
ivara obba sOdara praKyAta SrI em en kRuZhTarAyaru
aMdare oMdu aBiprAya A vAkyadalli pUrtiyAgi hELalAgide eMdu artha
pOrcugal dESada athava mUlada jana
AgrA nagara BArata dESada uttara pradESa rAjyadalliruva oMdu prasiddha pravAsi sthaLa
kailAsaM svataH atyaMta unnata maTTada SikZhaNa paDediddaru
Sukra idu sUrya nige eraDanE ati samIpada graha
sariyAda baLake yAvudu eMdu tiLidu elleDe adannE baLasuvudu uttama
rAjaratnaM avara sAhitya sEvege eraDu muKa
hiMdU dharmada cAMdramAna paMcAMgada modalanE mAsa
gaNES yArO mUranE vyaktiya hAgE kare mADi avarannu svalpa
dakZhiNa amErika KaMDada uttara BAgada oMdu dESa
kannaDa citraraMga da nirmApaka mattu nirdESakarallobbaru mAruti SivarAM
BArata da prasiddha baMgALi sinimA nirmApaka
innu idara bagge araLikaTTeyalli carce mADuvudu bEDa
pUrva EZhyA da oMdu rAZhTra
idu Sivamogga jilleya oMdu Uru
idara upayOgada bagge heccina mAhiti illa
heccina vivaragaLige klik mADi jOga
bEMdreyavara kRutigaLu dalita mattu baMDAya
idu idE hesarina tAlUkina kEMdravU haudu
beMgaLUru seMTral kAlEjinalli bi E padavi paDedaru
sEnA puraskAragaLu padavannu bEre lEKanagaLalli huDuki
illina muKya nadi hEmAvati muMde idu kAvEri nadiyannu sEruttade
modalu SrIraMgapaTTaNa dalli vakIli vRutti prAraMBisidaru
APrikAda saMpUrNa dakZhiNa BAgada pradESada bagge mAhitige dakZhiNa APrikA pradESa lEKana nODi
marAThi sAhityadalli illiyavarege mUvaru sAhitigaLu j~jAnapITha praSasti gaLisiddAre
dharmaSrI es el Bairappanavara prathama kRuti
mane tuMbida heNNu citrakke gItegaLannu racisuva avakASa laBisitu
uZhA navaratnarAM avara idE hesarina kAdaMbari AdhArita citra
idu madhya EZhyA da oMdu rAZhTravAgide
upEMdra kannaDa calanacitrada bagge mAhitige I lEKanavannu Odi